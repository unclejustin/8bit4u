module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      gridTemplateColumns: {
        // Simple 16 column grid
        16: 'repeat(16, minmax(0, 1fr))',
      },
    },
  },
  variants: {
    extend: {
      backgroundColor: ['active'],
      ringWidth: ['hover'],
    },
  },
  plugins: [require('@tailwindcss/forms')],
}
